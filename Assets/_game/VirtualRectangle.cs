﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualRectangle : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 _center, _scale;
    private float _qouaterOfX, _quouaterofY;

    public Vector3 Center { get => _center;}
    public Vector3 Scale { get => _scale;}
    public float QouaterOfX { get => _qouaterOfX;}
    public float QuouaterofY { get => _quouaterofY;}
    private void Awake()
    {
        SetData();
    }
    public void SetData()
    {
        _center = this.transform.localPosition;
        _scale = this.transform.localScale;
        _qouaterOfX = _scale.x / 4;
        _quouaterofY = _scale.y / 4;
    }
    public void SetData(Vector3 _skale)
    {
        _center = this.transform.localPosition;
        _scale = _skale;
        _qouaterOfX = _scale.x / 4;
        _quouaterofY = _scale.y / 4;
    }

}
