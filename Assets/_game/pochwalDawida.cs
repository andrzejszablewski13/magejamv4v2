﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pochwalDawida : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _growthPerTick=0.1f,_safeTimeBase=3f, _growthWinDifrence=4,_startTimeToSmaller=5,_speedSamlingBy=0.9f;
    private float _timeStart=0, _timeToSmaller,_timeBorn;
    private Animator _anim;
    private Vector3 _startScale;
    private Sequence mySequence;

    void Start()
    {
        mySequence = DOTween.Sequence();
        _timeToSmaller = _startTimeToSmaller;
        _anim = this.gameObject.GetComponentInChildren<Animator>();
        _startScale = this.transform.localScale;
        _timeBorn = Time.time;

        //mySequence.Kill();

    }

    // Update is called once per frame
    [SerializeField] private float _timeRest=0.1f;
    public void MasuMasu()
    {
        if( _timeStart+ _timeRest < Time.time)
        {
            GameMenager.Instance.DawidPoints++;
            this.transform.DOKill();
            _anim.SetBool("DawidSmile", true);
            _anim.Play("dawidReward2");
            Debug.Log(_anim.GetCurrentAnimatorStateInfo(0).length);
            _timeRest = _anim.GetCurrentAnimatorStateInfo(0).length;
            _timeStart = Time.time;
            this.transform.DOScale(this.transform.localScale *( 1 + _growthPerTick), _timeRest);
        }
    }
    private void FixedUpdate()
    {
        if(this.transform.localScale.x >=( _startScale.x * _growthWinDifrence))
        {
            this.transform.localScale = _startScale;
            this.transform.GetComponentInParent<Spawn4PreFabs>().SpawnCopies();
            
        }
        if(GameMenager.Instance.MaxDawidsNumber>1 && _timeStart + _safeTimeBase + GameMenager.Instance.MaxDawidsNumber/2 < Time.time && !mySequence.IsActive())
        {
            _anim.SetBool("DawidSmile", false);
            if (mySequence.Duration(false) <= 1)
            { mySequence.Append(this.transform.DOScale(Vector3.zero, _timeToSmaller).SetLoops(-1)); }
            else
            {
                mySequence.Play();
            }
        }
        if(this.transform.localScale.x <= 0.4f)
        {
            GameMenager.OnGameEnd();
            Debug.Log("lost");
        }
        if(_timeBorn+5<=Time.time)
        {
            _timeBorn = Time.time;
            _timeToSmaller *= _speedSamlingBy;
        }
    }
   
}
