﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControler : MonoBehaviour
{
    // Start is called before the first frame update
    private ParticleSystem _particle;
    void Start()
    {       
        _particle = this.gameObject.GetComponent<ParticleSystem>();
        _particle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        GameMenager.Instance.ParticleSystem = this;
    }

    public void ShowParticles(Vector3 _pos)
    {
        this.transform.position = _pos;
        _particle.Play();
    }
}
