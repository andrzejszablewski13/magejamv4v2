﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
using Lean.Touch;
using DG.Tweening;

public class Spawn4PreFabs : MonoBehaviour
{
    // Start is called before the first frame update
    public bool isSpawned = false;
    public bool goVertical = false;
    [HideInInspector]public VirtualRectangle _rectangle;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private int _maxSmallingRound=3;
    [HideInInspector] public int roundCounter = 0;
    public float _anTime = 0.5f;
    private Vector3 _destini;
    private bool _didit = false;
    private Vector3 _startScale = new Vector3(185, 100, 0);
    void Awake()
    {
        _rectangle = this.transform.GetComponent<VirtualRectangle>();
    }
    public void MoveTo(Vector3 _target,Vector3 _scale)
    {
        _destini = _target;
        if(roundCounter<_maxSmallingRound)
        {
            this.transform.DOScale(this.transform.localScale / 1.15f, 0.5f);
        }
        this.transform.DOLocalMove(_destini, _anTime).OnComplete(() => {
            this.GetComponent<VirtualRectangle>().SetData(_scale);
            this.GetComponent<Spawn4PreFabs>()._rectangle = this.GetComponent<VirtualRectangle>();
        });
        
    }
    // Update is called once per frame
    private Vector3 _newPos;
    public void SpawnCopies()
    {
        if (!_didit)
        {
            
            _didit = true;
            GameMenager.Instance.MaxDawidsNumber ++;
            GameMenager.Instance.KabuSource.Play();
            if(GameMenager.Instance.Monitor==null)
            {
                GameMenager.Instance.Monitor = this.transform.parent;
            }
            GameMenager.Instance.ParticleSystem.ShowParticles(this.transform.position);
            for (int i = 1; i < 3; i++)
            {
                
                var go = LeanPool.Spawn(_prefab, this.transform.position, Quaternion.identity, GameMenager.Instance.Monitor);
                go.GetComponent<Spawn4PreFabs>().roundCounter = roundCounter++;
                if (!goVertical)
                {
                    _newPos = new Vector3(_rectangle.Center.x + (i % 2 == 0 ? _rectangle.QouaterOfX : -_rectangle.QouaterOfX),
                      _rectangle.Center.y, _rectangle.Center.z);
                    if (Mathf.Abs(_newPos.x) <= _startScale.x / 2 && Mathf.Abs(_newPos.y) <= _startScale.y / 2)
                    {
                        go.GetComponent<Spawn4PreFabs>().MoveTo(_newPos,
                          roundCounter++< _maxSmallingRound?  new Vector3(_rectangle.Scale.x - _rectangle.QouaterOfX * 2, 
                        _rectangle.Scale.y, _rectangle.Scale.z):_rectangle.Scale); 
                    }else
                    {
                        LeanPool.Despawn(go);
                        continue;
                    }
                }else
                {
                    _newPos = new Vector3(_rectangle.Center.x,
                     _rectangle.Center.y + (i % 2 == 0 ? _rectangle.QuouaterofY : -_rectangle.QuouaterofY), _rectangle.Center.z);
                    if (Mathf.Abs(_newPos.x) <= _startScale.x / 2 && Mathf.Abs(_newPos.y) <= _startScale.y / 2)
                    {
                        go.GetComponent<Spawn4PreFabs>().MoveTo(_newPos,
                       roundCounter++ < _maxSmallingRound ? new Vector3(_rectangle.Scale.x,
                     _rectangle.Scale.y - _rectangle.QuouaterofY * 2, _rectangle.Scale.z): _rectangle.Scale);
                    }
                    else
                    {
                        LeanPool.Despawn(go);
                        continue;
                    }
                }
                go.GetComponent<LeanSelectable>().IsSelected = false;
                go.GetComponent<Spawn4PreFabs>().isSpawned = true;
                go.GetComponent<Spawn4PreFabs>().goVertical = !goVertical;
            }
            if (isSpawned)
            {
                LeanPool.Despawn(this.gameObject, _anTime);
            }
        }
    }

}
