﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenager : MonoBehaviour
{
    // Start is called before the first frame update
    private static GameMenager _instance;
    private int _dawidPoints = 0;
    private int _maxDawidsNumber=1;
    private int _recordDariwPoints, _recordDawirdsNumber;
    public  delegate void GameEnd();
    public static GameEnd OnGameEnd;
    private ParticleControler _particleSystem;
    public static GameMenager Instance { get => _instance;}
    public int DawidPoints { get => _dawidPoints; set => _dawidPoints = value; }
    public int MaxDawidsNumber { get => _maxDawidsNumber; set { _maxDawidsNumber = value; _fSoundTrack.ChangePar(_maxDawidsNumber); } }
    public bool InPl { get => _inPl; set => _inPl = value; }
    public ParticleControler ParticleSystem { get => _particleSystem; set => _particleSystem = value; }
    public int RecordDariwPoints { get => _recordDariwPoints; set => _recordDariwPoints = value; }
    public int RecordDawirdsNumber { get => _recordDawirdsNumber; set => _recordDawirdsNumber = value; }
    public FMODSoundtrack FSoundTrack { get => _fSoundTrack; }
    public bool FirstPlayEnded { get => _firstPlayEnded; set => _firstPlayEnded = value; }
    public AudioSource KabuSource { get => _kabuSource; }
    public Transform Monitor { get => _monitor; set => _monitor = value; }

    private FMODSoundtrack _fSoundTrack;
    private AudioSource _kabuSource;

    private bool _inPl;

    [SerializeField] private bool _firstPlayEnded = false;

    private Transform _monitor;
    void Awake()
    {
        
        if (_instance==null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
            _fSoundTrack = this.GetComponent<FMODSoundtrack>();
            _kabuSource = this.GetComponent<AudioSource>();
        }
        else
        {
            Destroy(this);
        }
    }

}
