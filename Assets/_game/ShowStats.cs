﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class ShowStats : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI _dawidPoints, _score, _endTitle, _buttonText, _dawidPointsH, _scoreH,_exit;
    [SerializeField] private string _scorePL, _scoreEng, _dawidpointsPL, _dawidPointsEng, _buttonTextPl, _buttonTextEng, _dawidPointsHEng, _dawidPointsHPL, _scoreHENG, _scoreHPL;
    [SerializeField] private string[] _endTitleEng, _endTitlePl;
    [SerializeField] private GameObject _back, _button, _title,_buttonExit;
    void Start()
    {
        GameMenager.OnGameEnd += ShowEndGame;
    }
    private void OnDisable()
    {
        GameMenager.OnGameEnd -= ShowEndGame;
    }
    private void ShowEndGame()
    {
        GameMenager.OnGameEnd -= ShowEndGame;
        Debug.Log(GameMenager.Instance.InPl);
        _dawidPoints.gameObject.SetActive(true);
        _score.gameObject.SetActive(true);
        _back.SetActive(true);
        _button.SetActive(true);
        _title.SetActive(true);
        _dawidPointsH.gameObject.SetActive(true);
        _scoreH.gameObject.SetActive(true);
        _buttonExit.SetActive(true);
        if (GameMenager.Instance.MaxDawidsNumber> GameMenager.Instance.RecordDawirdsNumber)
        {
            GameMenager.Instance.RecordDawirdsNumber = GameMenager.Instance.MaxDawidsNumber;
        }
        if(GameMenager.Instance.DawidPoints > GameMenager.Instance.RecordDariwPoints)
        {
            GameMenager.Instance.RecordDariwPoints = GameMenager.Instance.DawidPoints;
        }
        if (GameMenager.Instance.InPl)
        {
            _dawidPoints.text = _dawidpointsPL + " " + GameMenager.Instance.MaxDawidsNumber.ToString();
            _score.text = _scorePL + " " + GameMenager.Instance.DawidPoints.ToString();
            _buttonText.text = _buttonTextPl;
            _endTitle.text = _endTitlePl[Random.Range(0, _endTitlePl.Length)];
            _dawidPointsH.text= _dawidPointsHPL + " " + GameMenager.Instance.RecordDawirdsNumber.ToString();
            _scoreH.text = _scoreHPL + " " + GameMenager.Instance.RecordDariwPoints.ToString();
            _exit.text = "Wyjdż z gry.";
        }
        else
        {
            _dawidPoints.text = _dawidPointsEng + " " + GameMenager.Instance.MaxDawidsNumber.ToString();
            _score.text = _scoreEng + " " + GameMenager.Instance.DawidPoints.ToString();
            _buttonText.text = _buttonTextEng;
            _endTitle.text = _endTitleEng[Random.Range(0, _endTitlePl.Length)];
            _dawidPointsH.text = _dawidPointsHEng + " " + GameMenager.Instance.RecordDawirdsNumber.ToString();
            _scoreH.text = _scoreHENG + " " + GameMenager.Instance.RecordDariwPoints.ToString();
            _exit.text = "Close Game.";

        }
    }
}
