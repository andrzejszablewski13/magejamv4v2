﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using FMOD.Studio;

public class FMODSoundtrack : MonoBehaviour
{
    [SerializeField] private string _selectSoundtrack;
    [SerializeField] private int _audioBMPJump = 3;
    [SerializeField] private float _maxVolume = 0.8f;
    EventInstance soundtrack;

    private void Awake()
    {
        soundtrack = FMODUnity.RuntimeManager.CreateInstance(_selectSoundtrack);
        GameMenager.Instance.FSoundTrack.PlaySoundtrack(0);
        Silence();
    }
    public void ChangePar(float _headNumber)
    {
        soundtrack.setParameterByName("headNumber", _headNumber*_audioBMPJump);
        


    }
    public void Silence()
    {
        soundtrack.setVolume(0);
    }
    public void Loud()
    {
        soundtrack.setVolume(_maxVolume);
    }
    public void PlaySoundtrack(float _headNumber)
    {
        
        soundtrack.setParameterByName("headNumber", _headNumber);
        soundtrack.start();
    }
    private void Update()
    {

        
        if (Input.GetKeyDown(KeyCode.P))
        {
            soundtrack.start();
        }
    }
    public void StopSoundtrack()
    {
        soundtrack.stop(STOP_MODE.ALLOWFADEOUT);
        soundtrack.release();
    }
    private void OnDisable()
    {
        StopSoundtrack();
    }
}