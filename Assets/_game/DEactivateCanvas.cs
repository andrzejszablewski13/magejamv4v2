﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class DEactivateCanvas : MonoBehaviour
{
    private VideoPlayer _player;
    public void Start()
    {
        _player = this.gameObject.GetComponentInChildren<VideoPlayer>(true);
        if (!GameMenager.Instance.FirstPlayEnded)
        {
            _player.loopPointReached += StopFilmStartGame;
            GameMenager.Instance.FirstPlayEnded = true;

        }else
        {
            _player.enabled = false;
            _player.gameObject.transform.parent.gameObject.SetActive(false);
            GameMenager.Instance.FSoundTrack.Loud();
        }
    }
    public void OnDecided(bool InPl)
	{
		this.gameObject.SetActive(false);
        GameMenager.Instance.InPl = InPl;
        Debug.Log(GameMenager.Instance.InPl);
	}
    private void StopFilmStartGame(VideoPlayer vp)
    {
        _player.enabled = false;
        _player.gameObject.transform.parent.gameObject.SetActive(false);
        GameMenager.Instance.FSoundTrack.Loud();
    }
}
