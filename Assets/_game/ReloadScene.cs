﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{
    // Start is called before the first frame update
    public void REloadGAmeScene()
    {
        GameMenager.Instance.FSoundTrack.Silence();
        GameMenager.Instance.DawidPoints = 0;
        GameMenager.Instance.MaxDawidsNumber = 1;
        SceneManager.LoadScene(1);
        
    }
}
