﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class NewBehaviourScript : MonoBehaviour
{
	private VideoPlayer _player;
	public void Start()
	{
		_player = this.gameObject.GetComponentInChildren<VideoPlayer>(true);

			_player.loopPointReached += StopFilmStartGame;

		
	}

	private void StopFilmStartGame(VideoPlayer vp)
	{
		SceneManager.LoadScene(1);

	}
}
